object P90 extends App {

  type Board = List[Int]
  type Coords = (Int, Int)

  def eightQueens: Board =
    (1 to 8).toList
      .permutations.toStream
      .filter(checkDiagonals)
      .head

  def toCoords(b: Board): List[Coords] =
    b.zipWithIndex.map { case (row, idx) => (row, idx + 1) }

  def validDiagonals(coords: List[Coords]): Boolean = coords match {
    case Nil => true
    case (row, col) :: tail =>
      !tail.exists { case (nextRow, nextCol) =>
        Math.abs(row - nextRow) == Math.abs(col - nextCol)
      } && validDiagonals(tail)
  }

  val checkDiagonals: Board => Boolean = toCoords _ andThen validDiagonals


  val solution = eightQueens
  println(solution)
  printBoard(solution).foreach(println)

//  printBoard(List(1, 1, 1, 1, 1, 1, 1, 1)).foreach(println)


  def printBoard(b: Board): Seq[String] =
    (1 to 8)
      .map(row => (0 to 7)
          .map(icol =>
            if(b(icol) == row) 'Q' else '.'
          )
          .mkString(" ")
      )


}
