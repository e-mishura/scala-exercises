object TabsToSpaces extends App {

  val TAB = 't'
  val SPACE = 's'

  def tabsToSpaces(tabSize: Int, line: String): String =
    line.flatMap(c =>
      if (c == TAB) Seq.fill(tabSize)(SPACE)
      else Seq(c)
    )
    .mkString


    println(tabsToSpaces(2, "12s34t5st67"))
}
