import java.time.LocalDateTime

case class Event(timestamp: LocalDateTime, userId: Int, eventType: String)

object WebStats extends App {

  def eventCount(endTime: LocalDateTime, events: List[Event], eventType: String): Int =
    selectOneHour(endTime, events)
    .filter( _.eventType == eventType)
    .size


  def uniqueUsers(endTime: LocalDateTime, events: List[Event]): Int =
    selectOneHour(endTime, events)
    .map(_.userId)
    .foldLeft(Set.empty[Int]){ (acc, id) => acc + id }
    .size


  def selectOneHour(endTime: LocalDateTime, events: List[Event]): List[Event] = {
    val startTime = endTime.minusHours(1)
    events
      .dropWhile(_.timestamp.isAfter(endTime))
      .takeWhile(_.timestamp.isAfter(startTime))
  }


  val data = List(
    Event(LocalDateTime.of(2019, 7, 1, 14, 2), 1, "impression"),
    Event(LocalDateTime.of(2019, 7, 1, 14, 0), 1, "click"),
    Event(LocalDateTime.of(2019, 7, 1, 13, 59), 2, "click"),
    Event(LocalDateTime.of(2019, 7, 1, 13, 58), 3, "impression"),
    Event(LocalDateTime.of(2019, 7, 1, 13, 40), 1, "click"),
    Event(LocalDateTime.of(2019, 7, 1, 13, 29), 2, "impression"),
    Event(LocalDateTime.of(2019, 7, 1, 13, 21), 1, "click"),
    Event(LocalDateTime.of(2019, 7, 1, 12, 59), 1, "impression"),
  )


  println(eventCount(LocalDateTime.of(2019, 7, 1, 14, 30), data, "click"))
  println(eventCount(LocalDateTime.of(2019, 7, 1, 14, 30), data, "impression"))
  println(uniqueUsers(LocalDateTime.of(2019, 7, 1, 13, 50), data))
}


